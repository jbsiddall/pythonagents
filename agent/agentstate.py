from enum import Enum, unique
from typing import List


@unique
class AgentState(Enum):
    CREATED = 1
    INITIALISING = 2
    LISTENING = 3
    STOPPED = 4
    FINISHING = 5
    FINISHED = 6
    ERRORED = 7

    def __repr__(self) -> str:
        return self.name


RUNNING_STATES = frozenset([
    AgentState.CREATED,
    AgentState.INITIALISING,
    AgentState.LISTENING])
"all states that an agent is considered alive/running"
