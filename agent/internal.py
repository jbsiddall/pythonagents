"""provides an asynchronous message passing framework
"""

from collections import namedtuple, ChainMap
import queue
import uuid
import itertools
from queue import Queue
from functools import partial
from traceback import format_exc
from threading import Lock
from contextlib import contextmanager, suppress
from concurrent.futures import ThreadPoolExecutor
from threading import Thread
import sched
import time
import sys
import enum
from os.path import join
from types import TracebackType
from typing import (
        Dict,
        NamedTuple,
        Union,
        Callable,
        Any,
        Iterable,
        Tuple,
        List,
        Iterator,
        Optional,
        Type,
        cast,
        FrozenSet)
import logging
from logging import Logger

from . import metrics
from .metrics import DEFAULT_FORMATTTER
from .agentstate import AgentState, RUNNING_STATES

# types
ExcInfo = Tuple[Type[BaseException], BaseException, TracebackType]


def replace_logger_creator(creator: Callable[[str], Logger]) -> None:
    global logger_creator
    logger_creator = creator  # type: ignore


def logger_creator(logger_name: str) -> Logger:
    return logging.getLogger(logger_name)


def agent_logger(name: str) -> Logger:
    logger_name = 'agent.' + name
    return logger_creator(logger_name)


def str_func(*fields: str) -> Callable[[object], str]:
    def __str__(self: object) -> str:
        nonlocal fields
        if len(fields) == 1 and fields[0] is None:
            fields = self.__dict__.keys()
        vals = ["{}={}".format(f, getattr(self, f, None)) for f in fields]
        return self.__class__.__name__ + "(" + ", ".join(vals) + ")"
    return __str__


def init(*fields: str) -> Callable[..., None]:
    """returns an __init__ method that assigns fields as attributes"""

    def __init__(self: object, *args: Any, **kwargs: Any) -> None:

        # unknown kwargs
        unknown_kwargs = set(kwargs) - set(fields)
        if len(unknown_kwargs) > 0:
            raise NameError("unknown kwargs: " + ", ".join(unknown_kwargs))

        if len(args) + len(kwargs) > len(fields):
            expected, actual = len(fields), len(args) + len(kwargs)
            raise TypeError("""too many args supplied,
                    expecting {}, actual {}""".format(expected, actual))

        posargs = dict(zip(fields, args))
        namedargs = {f: kwargs[f] for f in fields[len(args):]}

        if len(set(posargs) & set(namedargs)) > 0:
            names = set(posargs) & set(namedargs)
            raise TypeError("""__init__ got multiple values for
                    "attributes: """ + ", ".join(names))

        final_args = ChainMap(posargs, namedargs)
        for field in fields:
            setattr(self, field, final_args[field])
    return __init__


Message = NamedTuple('Message', [
        ('id', str),
        ('sender', 'AgentRef'),
        ('event', str),
        ('payload', Any)])


class AppMessage(Message):
    pass


class ControlMessage(Message):
    pass


class AddAgent(ControlMessage):
    EVENT = 'add_agent'

    @staticmethod
    def payload(
            agent: 'Agent',
            before_kwargs: Dict[Any, Any]) -> Dict[str, Any]:
        return {'agent': agent, 'before': before_kwargs}


class Shutdown(ControlMessage):
    EVENT = 'shutdown'

    @staticmethod
    def payload() -> Dict[str, Any]:
        return {}


def agent_transition_control_message(
        from_state: AgentState,
        to_state: AgentState,
        details: Any=None) -> Dict[str, Any]:
    return dict(from_state=from_state, to_state=to_state, details=details)
agent_transition_control_message_event = 'agent_transition'


def message_id_generator(agent_name: str) -> Iterator[str]:
    rnd = str(uuid.uuid4())[-4:]
    return ("{}:{}:{}".format(agent_name, rnd, i) for i in itertools.count())


class AgentNotInContextError(ValueError):
    def __init__(self, agent: 'Agent') -> None:
        super().__init__("agent {} not in context".format(agent.name))
        self.agent = agent


class ActingAgent:

    """an agent that runs on it's own thread. it asynchronously
    handles messages. guarenteed that no 2 threads will touch
    the instance at the same time.
    handles app level messages as well as control messages from system.
    """

    def __init__(self,
                 system_ref: 'SystemRef',
                 agent_ref: 'AgentRef',
                 agent: 'Agent',
                 inbox: 'Inbox',
                 before_kwargs: Dict[str, Any],
                 logger: Logger,
                 metrics_logger: Callable[[metrics.Metric], None]) -> None:
        """creates a new ActingAgent tying it to an agent.
        system_ref: SystemRef  reference to the AgentSystem
        agent_ref: AgentRef  reference for this agent
        agent: Agent  agent that is bound to this ActingAgent
        inbox: Inbox  inbox for receiving messages
        before_kwargs: dict  kwargs to pass to before method
        """

        if agent.context is not None:
            raise NameError("""agent already has a property called 'context'.
                this is a special property that is set by ActingAgent.""")

        self.system_ref = system_ref
        self.agent_ref = agent_ref
        self.agent = agent
        self.inbox = inbox
        self.before_kwargs = dict(before_kwargs)
        self.message_id_generator = message_id_generator(self.agent.name)

        self.state = AgentState.CREATED
        self.state_start_time = time.monotonic()

        self.logger = logger
        self.metrics_logger = metrics_logger

    def _transition_state(
            self,
            new_state: AgentState,
            details: Any = None) -> None:
        assert isinstance(new_state, AgentState)

        now = time.monotonic()

        self.metrics_logger(metrics.TransitionMetric(
            from_state_start_time=self.state_start_time,
            to_state_start_time=now,
            agent=self.agent.name,
            from_state=self.state,
            to_state=new_state))

        old_state = self.state

        self.state = new_state
        self.state_start_time = now

        self.system_ref.send_message(ControlMessage(
            next(self.message_id_generator),
            self.agent_ref,
            agent_transition_control_message_event,
            agent_transition_control_message(old_state, new_state, details)))

    def run(self) -> None:
        """called on a new thread. starts the lifecycle of the agent.
        wraps the lifecycle so any exceptions handled and user notified.
        """
        try:
            self.lifecycle()
        except:
            self.logger.exception('unexpected error during lifecycle')

    def lifecycle(self) -> None:
        """runs this agent. stops when the agent stops"""

        @contextmanager
        def agent_context(message: Optional[Message] = None) -> Iterator:
            """context manager for setting and removing the context"""
            context = AgentContext(
                self.agent_ref,
                self.system_ref,
                message,
                self.message_id_generator,
                self.metrics_logger)
            setattr(self.agent, 'context', context)
            yield
            delattr(self.agent, 'context')
            if context.stopped:
                self._transition_state(AgentState.STOPPED)

        self._transition_state(AgentState.INITIALISING)

        try:
            with agent_context():
                self.agent.before(**self.before_kwargs)
        except Exception as e:
            info = cast(ExcInfo, sys.exc_info())
            self.logger.error('error in agent', exc_info=info)
            self._transition_state(AgentState.ERRORED, details=info)
        else:
            self._transition_state(AgentState.LISTENING)

        # listens for messages
        while self.state == AgentState.LISTENING:
            msg = self.inbox.read()
            if isinstance(msg, AppMessage):
                handler = self.agent.receive
            elif isinstance(msg, ControlMessage):
                handler = self.agent.receive_control

            self.__log_metric_received_message(msg)
            try:
                with agent_context(msg):
                    handler(msg.event, msg.payload)
            except Exception as e:
                info = cast(ExcInfo, sys.exc_info())
                self.logger.error('error in agent', exc_info=info)
                self._transition_state(AgentState.ERRORED, details=info)

        # calls after if stopped
        if self.state == AgentState.STOPPED:
            self._transition_state(AgentState.FINISHING)
            try:
                with agent_context():
                    self.agent.stopped()
            except Exception:
                info = cast(ExcInfo, sys.exc_info())
                self.logger.error('error in agent', exc_info=info)
                self._transition_state(AgentState.ERRORED, details=info)
            else:
                self._transition_state(AgentState.FINISHED)
        elif self.state == AgentState.ERRORED:
            try:
                with agent_context():
                    self.agent.errored()
            except Exception as e:
                exc_info = cast(ExcInfo, sys.exc_info())
                self.logger.error(
                    'error in agents errored() method',
                    exc_info=exc_info)

    def __log_metric_received_message(self, msg: Message) -> None:
        self.metrics_logger(metrics.ReceivedMessageMetric(
            message_id=msg.id,
            time_received=time.monotonic(),
            from_agent=msg.sender.name,
            to_agent=self.agent.name,
            event=msg.event))


class AgentContext(object):

    """a bridge from Agent to its ActingAgent"""

    def __init__(
            self,
            agent_ref: 'AgentRef',
            system_ref: 'SystemRef',
            message: Optional[Message],
            message_id_generator: Iterator[str],
            metrics_logger: Callable[[metrics.Metric], None]) -> None:
        """creates an Agentcontext.
        agent_ref: AgentRef  is the reference to the current agent in context
        system_ref: SystemRef  is the reference to the current agent system
        message: Message  is the message currently behing handled.
        """
        # TODO prefix these with _ (underscore) to emphesis private
        self.agent_ref = agent_ref
        self.system_ref = system_ref
        self.message = message
        self.message_id_generator = message_id_generator
        self.metrics_logger = metrics_logger
        self.stopped = False

    def respond(self, event: str, payload: Any = None) -> None:
        """responds to sender of current message.

        response will be the same level as the message being responded to.
        for example if system sends a control message then the response
        will also be a control message

        raises ValueError if context doesn't have a message to respond to. eg
        when agent's before method is being invoked
        """

        if self.message is None:
            raise ValueError('no message to respond to')

        self.send(
            self.message.sender,
            event,
            payload,
            control=isinstance(self.message.sender, ControlMessage))

    def send(
            self,
            receiver_ref: 'AgentRef',
            event: str,
            payload: Any,
            control: bool = False) -> None:
        assert self.message is not None
        assert event is not None
        assert receiver_ref is not None

        cls = ControlMessage if control else AppMessage
        msg_id = next(self.message_id_generator)
        receiver_ref.send_message(cls(
            msg_id,
            self.agent_ref,
            event,
            payload))

        self.metrics_logger(metrics.SentMessageMetric(
                message_id=msg_id,
                time_sent=time.monotonic(),
                from_agent=self.agent_ref.name,
                to_agent=receiver_ref.name,
                event=event))

    def stop(self) -> None:
        """stops the agent, not responding to any more messages."""
        self.stopped = True

    @property
    def sender(self) -> 'AgentRef':
        """returns AgentRef who sent the message
        raises ValueError if no message
        """
        if self.message is None:
            raise ValueError('no message in agent context')
        return self.message.sender

    def channel(self, receiver_ref: 'AgentRef') -> 'Channel':
        """returns a new channel from this agent to receiver_ref.
        can only be called when the agent is in context
        """
        return Channel(
            sender=self.agent_ref,
            sender_message_id_generator=self.message_id_generator,
            receiver=receiver_ref,
            metrics_logger=self.metrics_logger)


class Agent:

    """defines the behaviour of the ActingAgent.
    a special property will be set on it called "context" which will be
    an instance of AgentContext. allowing it to interact with ActingAgent
    """

    # name of agent. every instance can set this
    name = "Agent"  # type: str
    context = None  # type: Optional[AgentContext]

    def __init__(self, name: Optional[str] = None) -> None:
        if name is not None:
            assert isinstance(name, str)
            self.name = name

    # methods subclass can/should override
    def before(self, **kwargs: Dict[str, Any]) -> None:
        """called before the agent starts receiving messages.
        takes args and kwargs from those used to create agent.
        is overriden by sublcass if needed"""

    def receive(self, event: str, payload: Any) -> None:
        """receives a message from sender and responds according.
        receives a message from sender.
        params:
            event: str
            payload: object
        can raise a StopAgent to naturally stop the agent
        """
        raise NotImplementedError()

    def receive_control(self, event: str, payload: Any) -> None:
        if self.context is None:
            raise AgentNotInContextError(self)

        if Shutdown.EVENT == event:
            self.context.stop()

    def stopped(self) -> None:
        """called after agent has been stopped.
        will not be called if the agent stopped due to either error
        or agent received shutdown message
        """

    def errored(self) -> None:
        """called when an error in the agent's before/receive methods occur
        """

    # TODO depreciated. everyone should use self.context.channel
    def channel(self, receiver_ref: 'AgentRef') -> 'Channel':
        if self.context is None:
            raise AgentNotInContextError(self)
        return self.context.channel(receiver_ref)


class SimpleAgent(Agent):

    """a subclass of Agent that makes common patterns easier"""

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        # setup ref cache to prevent too much synchronous communication
        # only caches by name not by agent ref since these are free
        # cache is from agent name to agent_ref
        self._ref_cache = {}  # type: Dict[str, AgentRef]

    def channel(self, receiver: Union[str, 'AgentRef']) -> 'Channel':
        """allows receiver to be a string causing the ref to be fetched
        from system_ref
        """
        if isinstance(receiver, str):
            return self.__channel_for_agent_name(receiver)
        else:
            return self.__channel_for_agent_ref(receiver)

    def __channel_for_agent_name(self, name: str) -> 'Channel':
        if self.context is None:
            raise AgentNotInContextError(self)

        if name not in self._ref_cache:
            self._ref_cache[name] = self.context.system_ref.get_agent(name)
        return super().channel(self._ref_cache[name])

    def __channel_for_agent_ref(self, ref: 'AgentRef') -> 'Channel':
        if ref.name not in self._ref_cache:
            self._ref_cache[ref.name] = ref
        return super().channel(ref)

    def send_self(self, event: str, payload: Any = {}, delay: int = 0) -> None:
        """sends a message to itself
        (event, payload) is the message to send
        delay is the number of seconds to wait before sending it. 0 means
        don't wait.
        """
        assert delay >= 0

        if self.context is None:
            raise AgentNotInContextError(self)

        if delay == 0:
            channel = self.channel(self.context.agent_ref)
            channel.send(event, payload)
        else:
            channel = self.channel(ScheduleAgent.name)
            channel.send(*ScheduleAgent.schedule(delay=delay,
                                                 event=event,
                                                 payload=payload))


class EmptyInbox(Exception):

    """used by inbox and mailbox ref for indicating a empty inbox on reads
    when non blocking
    """


class Inbox:

    def __init__(self) -> None:
        self.inbox = Queue()  # type: Queue

    def post(self, message: Message) -> None:
        """posts the message to this inbox.
        message is the message to post. should be instance of Message
        blocks if inbox is full.
        """
        if not isinstance(message, Message):
            raise TypeError("message not an instance of Message")
        self.inbox.put(message)

    def read(self, block: bool = True) -> Message:
        """returns mail in inbox. blocks until there is mail to read.
        block: bool  blocks untill message in inbox if true otherwise raises
        EmptyInbox
        returns an instance of Message
        Throws EmptyInbox when block is True and inbox is empty.
        """
        try:
            msg = self.inbox.get(block=block)
        except queue.Empty as e:
            raise EmptyInbox from e

        assert isinstance(msg, Message)
        return msg


class AgentRef:

    def __init__(self, name: str, inbox: 'Inbox') -> None:
        """creates an AgentRef.
        name: str  name of the agent this refers to
        inbox: Inbox  the inbox of the agent. processes messages in the inbox
        """
        self.name = name
        self.inbox = inbox

    __str__ = __repr__ = str_func("name")

    def send_message(self, message: Message) -> None:
        """asynchronusly sends message to this agent.
        message should be instance of Message
        """
        self.inbox.post(message)

    def __eq__(self, other: Any) -> bool:
        return isinstance(other, AgentRef) and self.inbox == other.inbox

    def __hash__(self) -> int:
        return hash(self.inbox)


class MailboxRef:

    """a reference to an agent that allows synchronus communication.
    the Mailbox though it behaves like an agent, able to receive message is
    not an agent and therefore the system doesn't know about it.

    VERY IMPORTANT
    a MailboxRef should never be shared amoungst agents/threads. a new one
    should be created every time.
    """
    # TODO raise exception on blocking methods if agent errors half way through

    def __init__(self, name: str, agent_ref: 'AgentRef') -> None:
        """creates a MailboxRef.
        MailboxRef are for synchronous communication with a single agent.
        name: str  name of this mailbox
        agent_ref: AgentRef  is the ref to the agent to communicate with.
        mailbox_inbox: is the inbox for this mailbox. when the agent wants to
        respond to a message, the response will be sent to the mailbox_inbox.
        """
        self.agent_ref = agent_ref
        self.mailbox_ref = AgentRef(name, Inbox())
        self.message_id_generator = message_id_generator(name)

    @property
    def name(self) -> str:
        return self.mailbox_ref.name

    def send_message(self, message: Message) -> None:
        "asynchronously sends message"
        self.agent_ref.send_message(message)

    def send(self, event: str, payload: Any, control: bool = False) -> None:
        "asynchronously sends message"
        const = ControlMessage if control else AppMessage
        self.send_message(const(
            next(self.message_id_generator),
            self.mailbox_ref,
            event,
            payload))

    def read(self, block: bool = True) -> Tuple[str, Any]:
        """returns a message this mailbox has received.
        block: bool  blocks until message in inbox if true otherwise raises
        EmptyInbox
        throws EmptyInbox if block is True and mailbox is empty.
        returns (event, payload)
        """
        msg = self.mailbox_ref.inbox.read(block=block)
        event, payload = msg.event, msg.payload
        return (event, payload)

    def read_message(self) -> Message:
        return self.mailbox_ref.inbox.read()

    def send_message_wait(self, message: Message) -> Message:
        self.send_message(message)
        return self.read_message()

    def send_wait(
            self,
            event: str,
            payload: Any = None,
            control: bool = False) -> Tuple[str, Any]:
        """sends a message to the agent then waits for a reply"""
        payload = {} if payload is None else payload
        self.send(event, payload, control=control)
        return self.read()


class SystemShutdown(Exception):
    """thrown when an operation on ref performed after system shutdown"""
    pass


class SystemRef(MailboxRef):

    """ front end of the system.
    only method of communicating with the system. primarily synchronous.

    VERY IMPORTANT
    a SystemRef should NEVER be shared between agents. a new one should
    be created every time
    """

    def __init__(self, system: 'AgentSystem', system_inbox: 'Inbox') -> None:
        name = "mailbox-for-{}".format(system.name)
        super().__init__(name, AgentRef(system.name, system_inbox))
        self.system = system
        self.system_shutdown = False

    def add_agent(
            self,
            agent: 'Agent',
            before_kwargs: Dict[str, Any]={},
            mailbox: bool = True) -> Union['MailboxRef', 'AgentRef']:
        """synchronously adds agent
        if mailbox is True returns mailbox reference to agent
        otherwise returns AgentRef.
        args and kwargs are given to the agent's before method.
        blocks until agent has been added to system.
        """
        (event, payload) = self.send_wait(
            AddAgent.EVENT,
            AddAgent.payload(agent, before_kwargs),
            control=True)  # type: Tuple[str, Any]
        if AddAgent.EVENT != event:
            raise ValueError('received unexpected response: ' + event)
        elif not isinstance(payload, AgentRef):
            raise ValueError('{} expected payload to AgentRef not {}'.format(
                    AddAgent.EVENT,
                    str(type(payload))))
        elif mailbox:
            return self.get_mailbox(payload)
        else:
            return payload

    def add_agents(
            self,
            *agents: 'Agent',
            mailbox: bool=True) -> Union[List['AgentRef'], List['MailboxRef']]:
        """batch add agents simultanously.
        then will all be in the agentsystem before any of them start their
        lifecycle.
        returns list of agent_refs for all of them
        """
        (event, refs) = self.send_wait(
            'add_agents',
            {'agents': agents},
            control=True)
        if 'agents_added' != event:
            raise ValueError('received unexpected response: ' + event)
        elif (
                refs is None or
                not isinstance(refs, list) or
                not all(isinstance(x, AgentRef) for x in refs)):
            raise ValueError('expected payload for agents_added to be a list of AgentRefs instead of ' + str(type(refs)))  # noqa
        elif mailbox:
            return list(map(self.get_mailbox, refs))
        else:
            return refs

    def shutdown(self) -> None:
        """shutsdown the AgentSystem and waits for all agents to stop"""
        if self.system_shutdown:
            return
        event, _ = self.send_wait(Shutdown.EVENT,
                                  Shutdown.payload(),
                                  control=True)
        assert event == Shutdown.EVENT
        self.system_shutdown = True

    def get_mailbox(
            self,
            agent_ref: 'AgentRef',
            name: Optional[str] = None) -> MailboxRef:
        """returns a MailBoxRef for the given agent_ref.
        agent_ref: AgentRef  the agent to get a mailbox for.
        name: str optional name for this mailbox
        returns MailboxRef
        """
        assert isinstance(agent_ref, AgentRef)
        if name is None:
            name = "mailbox-for-{}".format(agent_ref.name)
        return MailboxRef(name, agent_ref)

    # TODO remove name_unique parameter. need to add assumption that all agent
    # names are unique and agent system will reject any agent that isn't unique
    def get_agent(self, name: str, name_unique: bool = True) -> 'AgentRef':
        """synchronusly queries system for agent with name then returns it's
        ref.
        name_unique   True if only one agent has name
        returns AgentRef
        raises ValueError if no agent with name or multiple agents have name
        but name_unique is True
        """
        result, refs = self.send_wait('get_agent',
                                      {'name': name})  # type: Tuple[str, Any]

        if 'get_agent_result' != result:
            raise ValueError('received unexpected response: ' + str(result))
        elif (
                refs is None or
                not isinstance(refs, list) or
                not all(isinstance(x, AgentRef) for x in refs)):
            raise ValueError('payload for get_agent_result expected to be list of AgentRef, not: ' + str(refs))  # noqa
        elif len(refs) == 0:
            raise ValueError('no agent with name {}'.format(name))
        elif len(refs) > 1 and name_unique:
            raise ValueError('multiple agents with name {}'.format(name))
        else:
            return cast('AgentRef', refs[0])


class Channel(object):

    """a unidirectional communication channel between two agents.
    allows easy message to an agent from a particular agent.
    """

    def __init__(
            self,
            sender: 'AgentRef',
            sender_message_id_generator: Iterator[str],
            receiver: 'AgentRef',
            metrics_logger: Callable[[metrics.Metric], None]) -> None:
        self.sender = sender
        self.sender_message_id_generator = sender_message_id_generator
        self.receiver = receiver
        self.metrics_logger = metrics_logger

    def send(self, event: str, payload: Any, control: bool = False) -> None:
        cls = ControlMessage if control else AppMessage
        msg_id = next(self.sender_message_id_generator)
        self.receiver.send_message(cls(
            id=msg_id,
            sender=self.sender,
            event=event,
            payload=payload))

        self.metrics_logger(metrics.SentMessageMetric(
                message_id=msg_id,
                time_sent=time.monotonic(),
                from_agent=self.sender.name,
                to_agent=self.receiver.name,
                event=event))


def create_system(**kwargs: Any) -> SystemRef:
    """creates an AgentSystem and returns a SystemRef"""
    system = AgentSystem(**kwargs)
    return system.system_ref


ErrorHandler = Callable[[SystemRef, AgentRef], None]


# TODO remove ability to have duplicate agents with same name.
# code in ref_cache already makes this assumption
class AgentSystem(Agent):

    def __init__(
            self,
            name: str = "system",
            max_threads: int=1000,
            error_handler: Optional[ErrorHandler]=None,
            metrics_formatter: Optional[metrics.MetricFormatter]=None) -> None:
        self.name = name
        self.logger = agent_logger(name)

        self.metric_logger_formatter = metrics_formatter or DEFAULT_FORMATTTER

        # all the refs ever created maps to their state.
        self.refs = {}  # type: Dict[AgentRef,AgentState]

        # thread pool
        self.pool = ThreadPoolExecutor(max_workers=max_threads)

        # set to cause AgentSystem to stop processing messages
        self.shutdown_signal = False

        # adds AgentSystem to system
        self.system_inbox = Inbox()
        self.system_ref = SystemRef(self, self.system_inbox)
        self.ref = self.system_ref.agent_ref

        # callback to handle agents who fail.
        # takes args: system_ref, agent_ref
        self.error_handler = error_handler or (lambda sys_ref, agent_ref: None)

        self.add_agent(agent=self,
                       inbox=self.system_inbox,
                       agent_ref=self.system_ref.agent_ref,
                       system_ref=self.system_ref)

    def before(self, **kwargs: Any) -> None:
        self.add_agent(ScheduleAgent())
        self.add_agent(PubSubAgent())

    # TODO rewrite this post refactor
    def receive(self, event: str, payload: Any) -> None:
        if self.context is None:
            raise AgentNotInContextError(self)

        if 'get_agent' == event:
            name = payload['name']
            results = []
            for ref in self.get_agents():
                if ref.name == name:
                    results.append(ref)
            self.context.respond('get_agent_result', results)

    def receive_control(self, event: str, payload: Any) -> None:
        if self.context is None:
            raise AgentNotInContextError(self)

        if Shutdown.EVENT == event:
            self.shutdown()
        elif AddAgent.EVENT == event:
            if self.shutdown_signal:
                self.context.respond(Shutdown.EVENT, Shutdown.payload())
            else:
                agent = payload['agent']
                before_kwargs = payload['before']
                ref = self.add_agent(agent, before_kwargs)
                self.context.respond(event, ref)
        elif 'add_agents' == event:
            self.context.respond('agents_added',
                                 self.add_agents(payload['agents']))
        elif agent_transition_control_message_event == event:
            self.agent_state_transitioned(self.context.sender,
                                          payload['from_state'],
                                          payload['to_state'],
                                          payload['details'])
        else:
            self.logger.warning('UNHANDLED CONTROL MESSAGE %s', event)

    def stopped(self) -> None:
        self.pool.shutdown(wait=False)

    def get_agents(
            self,
            states: FrozenSet[AgentState]=RUNNING_STATES) -> List[AgentRef]:
        """returns list of agent refs in state.
        doesn't include system_ref in the results.
        returns list of AgentRefs that match criteria
        """

        results = []
        for ref, ref_state in self.refs.items():
            if ref_state in states:
                if ref is not self.ref:
                    results.append(ref)
        return results

    def add_agent(
            self,
            agent: Agent,
            before_kwargs: Dict[str, Any]={},
            inbox: Optional[Inbox] = None,
            agent_ref: Optional[AgentRef] = None,
            system_ref: Optional[SystemRef] = None) -> AgentRef:
        """adds agent to the system.
        returns the agent's ref
        """

        inbox = inbox or Inbox()

        acting_agent = self._add_agent(
            system_ref or SystemRef(self, self.system_inbox),
            agent_ref or AgentRef(agent.name, inbox),
            agent,
            inbox,
            before_kwargs)

        self.pool.submit(acting_agent.run)
        return acting_agent.agent_ref

    def add_agents(self, agents: List[Agent]) -> List[AgentRef]:
        refs = []
        runs = []
        for agent in agents:
            inbox = Inbox()
            agent_ref = AgentRef(agent.name, inbox)

            acting_agent = self._add_agent(
                SystemRef(self, self.system_inbox),
                agent_ref,
                agent,
                inbox,
                before_kwargs={})
            refs.append(agent_ref)
            runs.append(acting_agent.run)

        for run in runs:
            self.pool.submit(run)
        return refs

    def _add_agent(
            self,
            system_ref: SystemRef,
            agent_ref: AgentRef,
            agent: Agent,
            inbox: Inbox,
            before_kwargs: Dict[str, Any]) -> ActingAgent:
        logger = agent_logger(agent.name)
        metrics_logger = partial(
            metrics.log_metric,
            self.metric_logger_formatter,
            logger)

        acting = ActingAgent(
            SystemRef(self, self.system_inbox),
            agent_ref,
            agent,
            inbox,
            before_kwargs,
            logger,
            metrics_logger)
        self.refs[agent_ref] = AgentState.CREATED
        return acting

    def shutdown(self) -> None:
        if self.context is None:
            raise AgentNotInContextError(self)

        self.logger.info('SHUTTING DOWN')
        if not self.shutdown_signal:
            self.shutdown_signal = True
            assert isinstance(self.context.sender, AgentRef)
            self.shutdown_sender = self.context.sender
        living_refs = self.get_agents()
        if 0 == len(living_refs):
            self.logger.info('SHUTTING DOWN IMMEDIATELY')
            self.send_shutdown_signal(self.shutdown_sender)
            self.context.stop()
        else:
            count = len(living_refs)
            self.logger.info(
                'shutting system down. waiting on {} agents'.format(count))
            for ref in living_refs:
                self.logger.info("  - {}".format(ref))
            for ref in living_refs:
                self.send_shutdown_signal(ref)
            self.send_shutdown_signal(self.system_ref.agent_ref)

    def send_shutdown_signal(self, ref: AgentRef) -> None:
        if self.context is None:
            raise AgentNotInContextError(self)

        self.context.send(
            receiver_ref=ref,
            event=Shutdown.EVENT,
            payload=Shutdown.payload(),
            control=True)

    def agent_state_transitioned(
            self,
            agent_ref: AgentRef,
            from_state: AgentState,
            to_state: AgentState,
            details: Any) -> None:
        self.logger.debug('AGENT {} TRANSITION FROM {} TO {}'.format(
            agent_ref.name,
            from_state,
            to_state))

        self.refs[agent_ref] = to_state

        if AgentState.ERRORED == to_state:
            self.agent_errored(agent_ref, exc_info=details)
        elif AgentState.FINISHED == to_state:
            self.agent_finished(agent_ref)

    def agent_errored(
            self,
            agent_ref: AgentRef,
            exc_info: Tuple[Any, Exception, Any]) -> None:
        self.logger.error('AGENT ERROR %s', agent_ref.name, exc_info=exc_info)

        try:
            self.error_handler(self.system_ref, agent_ref)
        except Exception as e:
            self.logger.error('ERROR WITH ERROR_HANDLER: ' + str(e))

    def agent_finished(self, agent_ref: AgentRef) -> None:
        if self.context is None:
            raise AgentNotInContextError(self)

        if self.shutdown_signal:
            self.context.send(
                receiver_ref=self.ref,
                event=Shutdown.EVENT,
                payload=Shutdown.payload(),
                control=True)


class ScheduleAgent(Agent):

    name = "schedule"

    SCHEDULE = 'schedule'

    @staticmethod
    def schedule(delay: int, event: str, payload: Any) -> Tuple[str, Any]:
        """returns (event, payload) for a schedule message to this Agent.
        """
        payload = {
            'delay': delay,
            'event': event,
            'payload': payload}
        return (ScheduleAgent.SCHEDULE, payload)

    def __init__(self) -> None:
        super().__init__()
        self.logger = agent_logger(self.name)
        self.scheduler = sched.scheduler(time.monotonic, time.sleep)

        def keep_alive() -> None:
            self.scheduler.enter(delay=3, priority=1, action=keep_alive)
        keep_alive()

        def run() -> None:
            self.scheduler.run()
        self.worker = Thread(target=run, daemon=True)

    def before(self, **kwargs: Dict[str, Any]) -> None:
        self.worker.start()

    def receive(self, event: str, payload: Any) -> None:
        if self.context is None:
            raise AgentNotInContextError(self)

        if self.SCHEDULE == event:
            recipient = self.context.sender
            delay = payload['delay']
            s_event = payload['event']
            s_payload = payload['payload']

            mailbox = MailboxRef(
                    'schedule-mailbox-for-event-' + s_event,
                    self.context.agent_ref)

            self.scheduler.enter(
                    delay=delay,
                    priority=1,
                    action=mailbox.send,
                    argument=('fire', dict(
                        recipient=self.context.sender,
                        event=s_event,
                        payload=s_payload)))

        elif 'fire' == event:
            recipient = payload['recipient']
            fire_event = payload['event']
            fire_payload = payload['payload']
            message_id = next(self.context.message_id_generator)
            sender = self.context.agent_ref
            recipient.send_message(AppMessage(
                id=message_id,
                sender=sender,
                event=fire_event,
                payload=fire_payload))
        else:
            self.logger.warning('unable to handle event %s', event)

    def stopped(self) -> None:
        def stopper() -> None:
            while 0 < len(self.scheduler.queue):
                for event in self.scheduler.queue:
                    with suppress(ValueError):
                        self.scheduler.cancel(event)
        self.scheduler.enter(delay=0, priority=10, action=stopper)


class PubSubAgent(Agent):

    name = 'pubsub'

    SUBSCRIBE = 'subscribe'
    """
    subscribes the sending agent to a given event.
    payload payload is the event to subscribe to
    """

    PUBLISH = 'publish'
    """
    publishes an event.
    payload is a dict with keys: event, payload.
    event is the event to publish, payload is the payload to send to all
    subscribers.
    """

    UNSUBSCRIBE = 'unsubscribe'
    """
    unsubscribes the sender agent from the given event.
    payload is the event to unsubscribe from
    """

    def __init__(self) -> None:
        super().__init__()
        self.logger = agent_logger(self.name)
        self.subscribers = {}  # type: Dict[str, List[AgentRef]]

    def receive(self, event: str, payload: Any) -> None:
        if self.context is None:
            raise AgentNotInContextError(self)

        if event == self.SUBSCRIBE:
            self.subscribe(event=payload, subscriber=self.context.sender)
        elif event == self.UNSUBSCRIBE:
            self.unsubscribe(event=payload, unsubscriber=self.context.sender)
        elif event == self.PUBLISH:
            if not isinstance(payload, dict):
                self.logger.error(
                    'publisher must provide a payload of type dict, not %s',
                    type(payload))
                return
            payload = dict(payload)
            if 'event' not in payload:
                self.logger.error(
                    'publisher must provide a payload dict with "event" key')
                return
            if 'payload' not in payload:
                self.logger.error(
                    'publisher must provide a payload dict with "payload" key')
                return
            self.publish(event=payload['event'], payload=payload['payload'])
        else:
            self.unhandled(event, payload)

    def subscribe(self, event: str, subscriber: AgentRef) -> None:
        self.logger.debug(
            'Agent %s subscribed to event %s',
            subscriber.name,
            event)
        self.subscribers.setdefault(event, []).append(subscriber)

    def unsubscribe(self, event: str, unsubscriber: AgentRef) -> None:
        if self.context is None:
            raise AgentNotInContextError(self)

        try:
            self.subscribers.get(event, []).remove(unsubscriber)
        except ValueError as e:
            self.context.respond(self.UNSUBSCRIBE, False)
        else:
            self.context.respond(self.UNSUBSCRIBE, True)

    def publish(self, event: str, payload: Any) -> None:
        if self.context is None:
            raise AgentNotInContextError(self)
        elif self.context.message is None:
            raise ValueError('no message to respond to')

        subscribers = self.subscribers.get(event, [])
        names = (x.name for x in subscribers)
        self.logger.debug(
            'publishing even "%s" to subscribers: %s',
            event,
            ', '.join(names))
        sender = self.context.message.sender
        for subscriber in subscribers:
            subscriber.send_message(AppMessage(
                id=next(self.context.message_id_generator),
                sender=sender,
                event=event,
                payload=payload))

    def unhandled(self, event: str, payload: Any) -> None:
        self.logger.debug('UNHANDLED "%s" "%s"', event, payload)
