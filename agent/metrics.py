from collections import namedtuple
from enum import Enum
from logging import Logger
from typing import Dict, NamedTuple, Union, Callable, Type, Set

from .agentstate import AgentState

Time = float

TransitionMetric = NamedTuple('TransitionMetric', [
    ('from_state_start_time', Time),
    ('to_state_start_time', Time),
    ('agent', str),
    ('from_state', AgentState),
    ('to_state', AgentState)])


SentMessageMetric = NamedTuple('SentMessageMetric', [
    ('message_id', str),  # globally unique message id
    ('time_sent', Time),
    ('from_agent', str),
    ('to_agent', str),
    ('event', str)])


ReceivedMessageMetric = NamedTuple('ReceivedMessageMetric', [
    ('message_id', str),
    ('time_received', Time),
    ('from_agent', str),
    ('to_agent', str),
    ('event', str)])


Metric = Union[TransitionMetric, SentMessageMetric, ReceivedMessageMetric]
MetricFormatter = Dict[Type[Metric], str]


human_formatter = {
    TransitionMetric: "TRANSITION {agent}: {from_state} --> {to_state}",
    SentMessageMetric: "SENT {from_agent} --> {event} --> {to_agent}",
    ReceivedMessageMetric: "RECEIVED {from_agent} --> {event} --> {to_agent}",
}  # type: MetricFormatter

csv_formatter = {
    TransitionMetric: "transition,{agent},{from_state},{from_state_start_time},{to_state},{to_state_start_time}",  # noqa
    SentMessageMetric: "sent,{message_id},{time_sent},{from_agent},{to_agent},{event}",  # noqa
    ReceivedMessageMetric: "received,{message_id},{time_received},{from_agent},{to_agent},{event}"  # noqa
}  # type: MetricFormatter

METRICS = {
    TransitionMetric,
    SentMessageMetric,
    ReceivedMessageMetric}  # type: Set[Type[Metric]]
DEFAULT_FORMATTTER = csv_formatter


def log_metric(
        formatter: MetricFormatter,
        logger: Logger,
        metric: Metric) -> None:
    assert type(metric) in METRICS
    template = formatter[type(metric)]
    log_message = template.format(**metric._asdict())
    logger.debug(log_message)
