
from setuptools import setup

setup(
    name='pyagents',
    version='2.0.1',
    author='Joseph Siddall',
    author_email='j.b.siddall@gmail.com',
    license='BSD',
    packages=['agent'])
