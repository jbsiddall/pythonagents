import pytest
from pytest import list_of

import agent
from testutil import configure_logging, agent_system, ReplyAgent


def setup_module(module):
    configure_logging()


AGENTS = ["agent_{}".format(i) for i in range(100)]


@pytest.mark.randomize(positive=True, ncalls=3, choices=AGENTS)
@pytest.mark.timeout(10)
def test_agents_successfully_send_and_receive_messages(
        agent_system,
        messages: list_of([str, str, int, int, bool],
                          min_items=1,
                          max_items=1000)):

    def group_by(iter, k):
        return {
            key: [i for i in iter if k(i) == key]
            for key in set(map(k, iter))}

    send_messages = group_by(messages, lambda msg: msg[0])
    received_messages = group_by(messages, lambda msg: msg[1])
    agents = [ReplyAgent(name, send_messages.get(name, [])) for name in AGENTS]
    agent_mailboxes = agent_system.add_agents(*agents)

    for agent in agents:
        agent.ready.get()

    agent_system.shutdown()

    agent_lookup = dict(zip(AGENTS, agents))

    for receiver_agent, messages in received_messages.items():
        for (sender, receiver, event, payload, control) in messages:
            received_msgs = agent_lookup[receiver_agent].received
            assert (sender, event, payload, control) in received_msgs
