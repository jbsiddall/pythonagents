import queue
import pytest
from pytest import list_of
import logging

import agent

from testutil import configure_logging, agent_system


def setup_module(module):
    configure_logging()


def test_pubsub(agent_system):
    pubsub_ref = agent_system.get_agent(agent.PubSubAgent.name)
    pubsub = agent_system.get_mailbox(pubsub_ref)

    pubsub.send('subscribe', 'ping')
    pubsub.send('publish', {'event': 'ping', 'payload': 'testpayload'})

    actual_event, actual_payload = pubsub.read()

    assert 'ping' == actual_event
    assert 'testpayload' == actual_payload
