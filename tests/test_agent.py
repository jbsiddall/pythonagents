import pytest
from functools import partial
from collections import namedtuple

from testutil import agent_system, configure_logging
import agent
from agent import AgentState
from agent.metrics import (
    TransitionMetric,
    SentMessageMetric,
    ReceivedMessageMetric)
import agent.metrics


def setup_module(module):
    configure_logging()


# TODO move to test_system_ref.py?
def test_agent_before_method_called_with_kwargs(agent_system):
    before_called = []

    class BeforeAgent(agent.Agent):

        def before(self, **kwargs):
            before_called.append(kwargs)

    agent_system.add_agent(
            BeforeAgent('testagent'),
            before_kwargs=dict(key1='val1', key2='val2'))
    assert [dict(key1='val1', key2='val2')] == before_called


def test_when_error_occurs_in_agent_receive_method_then_transition_to_errored(
        mocker):
    logger = agent.agent_logger('myagent')
    metrics_logger = mocker.Mock()

    error = [None]

    class FailAgent(agent.Agent):
        def receive(self, event, payload):
            error[0] = ValueError()
            raise error[0]

    myagent = FailAgent('myagent')

    system = mocker.Mock()

    system_inbox = agent.Inbox()
    system_ref = agent.SystemRef(system, system_inbox)
    agent_inbox = agent.Inbox()
    agent_ref = agent.AgentRef('myagent', agent_inbox)

    acting_agent = agent.ActingAgent(
        system_ref,
        agent_ref,
        myagent,
        agent_inbox,
        {},
        logger,
        metrics_logger)
    mocker.spy(acting_agent, '_transition_state')

    agent_inbox.post(agent.AppMessage(
        'system:0',
        system_ref.agent_ref,
        "testevent",
        "testpayload"))

    acting_agent.run()

    transitions = acting_agent._transition_state.call_args_list
    init_args, listening_args, errored_args = transitions
    assert ((AgentState.INITIALISING,),) == init_args
    assert ((AgentState.LISTENING,),) == listening_args
    assert (AgentState.ERRORED,) == errored_args[0]
    assert error[0] is errored_args[1]['details'][1]

    metric_type_logged = [type(x[0][0]) for x in metrics_logger.call_args_list]
    assert [
        TransitionMetric,
        TransitionMetric,
        ReceivedMessageMetric,
        TransitionMetric] == metric_type_logged


def test_when_agent_errors_in_before_method_then_agent_transitions_to_errored(
        mocker,
        agent_system):
    error = [None]

    class FailAgent(agent.Agent):
        def before(self, **kwargs):
            error[0] = ValueError()
            raise error[0]

    myagent = FailAgent('myagent')
    env = create_acting_agent_environment(mocker, myagent)

    env.acting_agent.run()

    transition = env.acting_agent._transition_state.call_args_list
    trans_init, trans_errored = transition

    assert ((AgentState.INITIALISING,),) == trans_init
    assert (AgentState.ERRORED,) == trans_errored[0]
    assert error[0] == trans_errored[1]['details'][1]

    metrics = env.metrics_logger.call_args_list
    metric_init, metric_errored = [x[0][0] for x in metrics]
    assert "myagent" == metric_init.agent
    assert AgentState.CREATED == metric_init.from_state
    assert AgentState.INITIALISING == metric_init.to_state

    assert "myagent" == metric_errored.agent
    assert AgentState.INITIALISING == metric_errored.from_state
    assert AgentState.ERRORED == metric_errored.to_state


def test_when_system_shutsdown_then_agent_transitions_to_finished(mocker):
    myagent = agent.Agent('myagent')
    environment = create_acting_agent_environment(mocker, myagent)

    environment.agent_inbox.post(agent.ControlMessage(
            'system:0',
            environment.system_ref.agent_ref,
            agent.Shutdown.EVENT,
            agent.Shutdown.payload()))

    environment.acting_agent.run()

    transitions = environment.acting_agent._transition_state.call_args_list
    assert [((AgentState.INITIALISING,),),
            ((AgentState.LISTENING,),),
            ((AgentState.STOPPED,),),
            ((AgentState.FINISHING,),),
            ((AgentState.FINISHED,),)] == transitions

    metrics = [x[0][0] for x in environment.metrics_logger.call_args_list]
    (_, _, receive_metric, _, _, _) = metrics
    assert "system:0" == receive_metric.message_id
    assert "system" == receive_metric.from_agent
    assert "myagent" == receive_metric.to_agent
    assert "shutdown" == receive_metric.event


def create_acting_agent_environment(mocker, myagent):
    Environment = namedtuple('Environment', [
        'acting_agent',
        'metrics_logger',
        'system_inbox',
        'agent_inbox',
        'system_ref'])
    logger = agent.agent_logger(myagent.name)
    metrics_logger = mocker.Mock()

    system = mocker.Mock()
    system.name = "system"

    system_inbox = agent.Inbox()
    system_ref = agent.SystemRef(system, system_inbox)
    agent_inbox = agent.Inbox()
    agent_ref = agent.AgentRef(myagent.name, agent_inbox)

    acting_agent = agent.ActingAgent(
        system_ref,
        agent_ref,
        myagent,
        agent_inbox,
        {},
        logger,
        metrics_logger)
    mocker.spy(acting_agent, '_transition_state')

    return Environment(
        acting_agent,
        metrics_logger,
        system_inbox,
        agent_inbox,
        system_ref)
