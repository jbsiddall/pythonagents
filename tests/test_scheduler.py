import pytest
import time
import testutil
from testutil import agent_system
import agent
from agent import AgentState
from agent.metrics import (
    TransitionMetric,
    SentMessageMetric,
    ReceivedMessageMetric)
import agent.metrics
import agent.internal


def setup_module(module):
    testutil.configure_logging()


@pytest.mark.timeout(5)
def test_when_event_scheduled_it_will_be_fired(mocker, agent_system):
    scheduler = agent_system.get_mailbox(agent_system.get_agent('schedule'))

    start = time.monotonic()
    event, payload = scheduler.send_wait(
            agent.internal.ScheduleAgent.SCHEDULE,
            dict(delay=1, event='responseevent', payload='responsepayload'))
    duration = time.monotonic() - start
    assert 'responseevent' == event
    assert 'responsepayload' == payload
    assert duration >= 1
