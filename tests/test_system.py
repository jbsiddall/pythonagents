import pytest
import queue
from functools import partial
from collections import namedtuple

import testutil
from testutil import agent_system
import agent
from agent import AgentState
from agent.metrics import (
    TransitionMetric,
    SentMessageMetric,
    ReceivedMessageMetric)
import agent.metrics
import agent.internal


def setup_module(module):
    testutil.configure_logging()


def nope_sytem_agent_starts(mocker):
    mocker.patch('agent.internal.ThreadPoolExecutor')
    mocker.patch('agent.internal.ActingAgent')

    error_handler = mocker.Mock()
    system_ref = agent.internal.create_system()

    running_agents = agent.internal.ThreadPoolExecutor().submit.call_count
    assert 1 == running_agents, "system agent not added to threadpool"
    assert "system" == agent.internal.ActingAgent.call_args[0][1].name


def test_when_agent_fails_then_error_handler_called(mocker):
    class BeforeFailAgent(agent.internal.Agent):
        def before(self):
            raise ValueError()

    class ReceiveFailAgent(agent.internal.Agent):
        def receive(self, event, payload):
            raise ValueError()

    errors = queue.Queue()

    def error_handler(sys_ref, agent_ref):
        errors.put(agent_ref)

    with testutil.system(error_handler=error_handler) as system_ref:
        ref1 = system_ref.add_agent(BeforeFailAgent('before'), mailbox=False)
        mailbox2 = system_ref.add_agent(
                ReceiveFailAgent('before'),
                mailbox=True)
        mailbox2.send('test', 'test')
        assert {ref1, mailbox2.agent_ref} == {errors.get(), errors.get()}

        assert AgentState.ERRORED == system_ref.system.refs[ref1]
        assert AgentState.ERRORED == system_ref.system.refs[mailbox2.agent_ref]


def test_when_unknown_message_is_sent_to_system_then_it_discards(
        mocker,
        agent_system):
    mocker.patch('agent.internal.agent_logger')
    agent_system.send('testevent', 'testpayload', control=True)
