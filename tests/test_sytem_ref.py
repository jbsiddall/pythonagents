import pytest
import agent
from functools import partial

from testutil import agent_system, configure_logging, ReplyAgent


def setup_module(module):
    configure_logging()


# TEST method: add_agent
def test_when_add_agent_call_returns_then_agent_is_in_system(agent_system):
    ref = add_agent(agent_system, 'myagent')
    # concurrent access to refs dangerous
    assert ref in agent_system.system.refs


# TEST method: add_agents
def test_when_add_agents_called_then_all_agents_in_system(agent_system):
    refs = {add_agent(agent_system, 'myagent1'),
            add_agent(agent_system, 'myagent2')}
    # concurrent access to refs dangerous
    assert refs.issubset(agent_system.system.refs)


# TEST method: shutdown
def test_agent_system_can_but_shutdown_multiple_times():
    agent_system = agent.create_system()
    agent_system.shutdown()
    agent_system.shutdown()


def test_get_mailbox(agent_system):
    class EchoAgent(agent.Agent):
        def receive(self, event, payload):
            self.context.respond(event, payload)
    ref = agent_system.add_agent(EchoAgent('myagent'), mailbox=False)
    mailbox = agent_system.get_mailbox(ref)
    assert ('testevent', 'testpayload') == mailbox.send_wait('testevent',
                                                             'testpayload')


# TEST method: get_agent
def test_when_get_agent_called_then_correct_ref_returned(agent_system):
    original_ref = add_agent(agent_system, 'myagent')
    assert original_ref == agent_system.get_agent('myagent')


# TODO agent name should be unique and not possible for multiple agents to
# share same name
def test_when_agent_names_not_unique_then_get_agents_returns_one_of_them(
        agent_system):
    add = partial(add_agent, agent_system, 'myagent')
    agent_refs = {add(), add()}
    assert agent_system.get_agent('myagent', name_unique=False) in agent_refs


def add_agent(agent_system, name):
    return agent_system.add_agent(ReplyAgent(name), mailbox=False)


def test_calling_get_agent_throws_error_when_agent_doesnt_exist(agent_system):
    with pytest.raises(ValueError) as excinfo:
        agent_system.get_agent('fakeagent')
    assert str(excinfo.value) == 'no agent with name fakeagent'
