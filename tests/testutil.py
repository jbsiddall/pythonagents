import pytest
import logging
import queue
from contextlib import contextmanager

import agent


@pytest.fixture
def agent_system(request):
    system = agent.create_system()
    request.addfinalizer(system.shutdown)
    return system


@contextmanager
def system(**kwargs):
    system = agent.create_system(**kwargs)
    try:
        yield system
    finally:
        system.shutdown()


def configure_logging():
    root = logging.getLogger()
    root.setLevel(logging.INFO)
    ch = logging.FileHandler('agent.log')
    ch.setLevel(logging.DEBUG)
    root.addHandler(ch)


class ReplyAgent(agent.SimpleAgent):

    def __init__(self, name, messages_to_send=[]):
        super().__init__(name)
        self.ready = queue.Queue()
        self.received = []
        self.messages_to_send = messages_to_send

    def before(self):
        for (from_, to_, event, payload, control) in self.messages_to_send:
            self.channel(to_).send(event, payload, control)
        self.ready.put(None)

    def receive(self, event, payload):
        msg = (self.context.message.sender.name, event, payload, False)
        self.received.append(msg)

    def receive_control(self, event, payload):
        super().receive_control(event, payload)
        msg = (self.context.message.sender.name, event, payload, True)
        self.received.append(msg)
